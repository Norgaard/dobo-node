# DOBO For Node.js

Repo for Workshop: DevOps, best Ops.

## Firebase
Run commands from VS Code terminal for a better experience.

### Test deploy from machine

```sh
$ npm i -g firebase-tools
$ firebase login
$ firebase init
$ firebase deploy
```

### Setup deploy from CI
Save token from the login:ci part.

```sh
$ npm i firebase-tools --save-dev
$ firebase login:ci
```

Do this from CI

```sh
$ firebase deploy --token=$FIREBASE_TOKEN
```
